
<!-- README.md is generated from README.Rmd. Please edit that file -->

# phenokde <img src="man/figures/logo.png" align="right" height="138" />

<!-- badges: start -->
<!-- badges: end -->

This package provides facilities to compute Utilization Distributions
(UD) from Kernel Density Estimates (KDE) from geolocation data of
individuals `kde_computation()`, and to compute these KDE at
intra-specific levels (across Colonies) and across various time periods
(phenological states; `processing_pheno_kde()`).
